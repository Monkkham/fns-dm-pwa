<div wire:poll.10s>

    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    ປະເພດອາຫານ
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- categories -->
            <div class="categories segments-page">
                <div class="container">
                    @foreach ($category as $item)
                        <div class="content">
                            <a onclick="document.location='/menu/{{ $item->id }}'" >
                                <img src="https://cdn.britannica.com/36/123536-050-95CB0C6E/Variety-fruits-vegetables.jpg" alt="">
                                <div class="mask"></div>
                                <div class="title">
                                    <h4>{{ $item->name }}</h4>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- end categories -->
        </div>
    </div>

</div>
