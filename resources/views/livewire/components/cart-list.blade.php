<div wire:poll.10s>

    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/menu/0'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    ລາຍລະອຽດ
                </div>
            </div>
        </div>
        <div class="page-content">
            @if (count($checkold) > 0)
                <div class="blog segments-page">
                    <div class="container">
                        <div class="title">ລາຍການອາຫານ</div>
                        @foreach ($cartData as $item)
                            @if (isset($item->id))
                                <div class="row">
                                    <div class="col-30">
                                        <div class="content-text">
                                            <h4>{{ $item->name }}</h4>
                                            <small>{{ $item->price }} $</small>
                                        </div>
                                    </div>
                                    <div class="col-50">
                                        <div class="row">
                                            <div class="col-40">
                                                <button
                                                    wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ -1 }})">
                                                    -
                                                </button>
                                            </div>
                                            <div class="col-20">
                                                <input style="text-align: center" type="number" value="{{ $item->qty }}"
                                                    style="border: 1px solid rgb(0, 0, 0);width: 100%;">
                                            </div>
                                            <div class="col-40">
                                                <button
                                                    wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ 1 }})">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-20">
                                        {{ $item->qty * $item->price }} $
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="title">
                            ລວມທັງໝົດ: {{ $cartTotal }} $
                        </div>
                    </div>
                </div>
                <div class="page-content">
                    <!-- popular menu -->
                    <div class="popular-menu segments-page">
                        <div class="container">
                            <div class="list">
                                <h4>ສັ່ງເພີ່ມໃຫ້ລາຍການ</h4>
                                <div class="item-input-wrap">
                                    <select wire:model='oldId' style="border: 1px solid rgb(0, 0, 0);width: 100%;">
                                        <option selected>ເລືອກລາຍການ</option>
                                        @foreach ($checkold as $item)
                                            <option value="{{ $item->id }}">{{ $item->code }}</option>
                                        @endforeach
                                    </select>
                                    @error('oldId')
                                        <div class="content-text" style="color: red">
                                            ກະລຸນາໃສ່ຈຳນວນຄົນ...
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-100"><button wire:click='_checadd' class="button">ສັ່ງເພີ່ມເລີຍ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end popular menu -->
                </div>
            @else
                <!-- blog -->
                <div class="blog segments-page">
                    <div class="container">
                        <div class="title">ລາຍການອາຫານ</div>
                        @foreach ($cartData as $item)
                            @if (isset($item->id))
                                <div class="row">
                                    <div class="col-30">
                                        <div class="content-text">
                                            <h4>{{ $item->name }}</h4>
                                            <small>{{ $item->price }} $</small>
                                        </div>
                                    </div>
                                    <div class="col-50">
                                        <div class="row">
                                            <div class="col-40">
                                                <button
                                                    wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ -1 }})">
                                                    -
                                                </button>
                                            </div>
                                            <div class="col-20">
                                                <input type="number" value="{{ $item->qty }}"
                                                    style="border: 1px solid rgb(0, 0, 0);width: 100%;">
                                            </div>
                                            <div class="col-40">
                                                <button
                                                    wire:click="updateCart({{ $item->qty }},'{{ $item->rowId }}', {{ 1 }})">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-20">
                                        {{ $item->qty * $item->price }} $
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="title">ລາຍການໂຕະ</div>
                        <div class="row">
                            <div class="ol-100">
                                <div class="content-text">
                                    <h4>{{ var_export(count($chosetable)) }}</h4>
                                </div>
                            </div>
                            {{-- validate chosetable --}}
                            @if (count($chosetable) == 0)
                                <div class="col-100">
                                    <div class="content-text" style="color: red">
                                        ກະລຸນາເລືອກໂຕະ...
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="title">
                            ລວມທັງໝົດ: {{ $cartTotal }} $ + ຄ່າຈອງ: 100 000.00 $
                        </div>
                    </div>
                </div>
                <div class="page-content">
                    <!-- popular menu -->
                    <div class="popular-menu segments-page">
                        <div class="container">
                            <div class="row">
                                @foreach ($table as $item)
                                    <div class="col-20">
                                        <div class="content">
                                            <img src="https://www.freepnglogos.com/uploads/table-png/download-table-png-image-png-image-pngimg-39.png"
                                                alt="">
                                            <div class="text">
                                                <h4>{{ $item->code }}</h4>
                                                <span>ນັ່ງໄດ້: {{ $item->chiar_qty }}</span>
                                                <label class="item-checkbox item-content no-ripple">
                                                    <input wire:model='chosetable' value="{{ $item->id }}"
                                                        type="checkbox">
                                                    <i class="icon icon-checkbox"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="list">
                                <h4>ລາຍລະອຽດເພີ່ມຕື່ມ</h4>
                                <div class="item-input-wrap">
                                    <input wire:model='qtyp' type="number" placeholder="ຈຳນວນຄົນ" min="1"
                                        max="5" required>
                                    @error('qtyp')
                                        <div class="content-text" style="color: red">
                                            ກະລຸນາໃສ່ຈຳນວນຄົນ...
                                        </div>
                                    @enderror
                                </div>
                                <div class="item-input-wrap">
                                    <input wire:model='datetime' type="datetime-local" placeholder="ເວລາ" required>
                                    @error('datetime')
                                        <div class="content-text" style="color: red">
                                            ກະລຸນາໃສ່ເວລາ...
                                        </div>
                                    @enderror
                                </div>
                                <div class="item-input-wrap">
                                    <input wire:model='payment' type="file" accept="image/png, image/jpeg" required>
                                    @error('payment')
                                        <div class="content-text" style="color: red">
                                            ກະລຸນາໃສ່ຮູບການຈ່າຍ...
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-50"><button wire:click='_checkout({{ 1 }})'
                                        class="button"
                                        style="background-color: rgb(0, 170, 203)">ສັ່ງຈ່ອງເລີຍ</button></div>
                                <div class="col-50"><button wire:click='_checkout({{ 2 }})'
                                        class="button">ສັ່ງຊື້ເລີຍ</button></div>
                            </div>
                            <br>
                            <img src="https://res.cloudinary.com/dojyijib7/image/upload/v1686717651/jawxfsov4nf40ic6xfzg.jpg"
                                alt="" style="height: 500px;">
                        </div>
                    </div>
                    <!-- end popular menu -->
                </div>
            @endif
        </div>

    </div>

</div>
