<div wire:poll.10s>
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="title">
                    ຮ້ານອາຫານດາວອັງຄານ ຍິນດີຕ້ອນຮັບ
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- login -->
            <div class="login segments-page">
                <div class="container">
                    <div class="logos">
                        <div class="image">
                            <img src="images/logo.png" alt="">
                        </div>
                    </div>
                    <div class="list">
                        <div class="item-input-wrap">
                            <input wire:model='name' type="text" placeholder="ຊື່ ນາມສະກຸນ"
                                @error('address') is-invalid @enderror>
                            @error('name')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='phone' type="number" minlength="11" placeholder="ເບີໂທ: 8 ຕົວເລກ"
                                @error('phone') is-invalid @enderror>
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        {{-- <div class="form-group">
                            <label>ເລືອກແຂວງ</label>
                            <select wire:model="province_id" id="select1"
                                class="form-control
                             @error('province_id') is-invalid @enderror">
                                <option value="" selected>{{ __('lang.select') }}</option>
                                @foreach ($province as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                @endforeach
                            </select>
                            @error('province_id')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div> --}}
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='password' type="number" minlength="11" placeholder="ລະຫັດຜ່ານ"
                                @error('password') is-invalid @enderror>
                            @error('password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="item-input-wrap">
                            <input wire:model='confirm_password' type="number" minlength="11"
                                placeholder="ຍືນຍັນລະຫັດຜ່ານ" @error('address') is-invalid @enderror>
                            @error('confirm_password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <button wire:click='register' class="button"><i class="ti-shift-right"></i>ລົງທະບຽນ</button>
                    </div>
                    <div class="login-with">
                        <p>ເຂົ້າສູ່ລະບົບດ້ວຍ</p>
                        <ul>
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-google"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end login -->
        </div>
    </div>

</div>
