<div wire:poll.10s>
    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="title">
                    ຮ້ານອາຫານດາວອັງຄານ ຍິນດີຕ້ອນຮັບ
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- login -->
            <div class="login segments-page">
                <div class="container">
                    <div class="logos">
                        <div class="image">
                            <img src="images/logo.png" alt="">
                        </div>
                    </div>
                    <div class="list">
                        <div class="item-input-wrap">
                            <input wire:model='phone' type="number" minlength="8" placeholder="ເບີໂທ ex:20xxxxxxxx"
                                required>
                                @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <button wire:click='_customerLong' class="button"><i
                                class="ti-shift-right"></i>ເຂົ້າສູ່ລະບົບ</button>
                    </div>
                </div>
            </div>
            <!-- end login -->
        </div>
    </div>

</div>
