<div wire:poll>

    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    @if ($checkback == 0)
                        <a onclick="document.location='/'" class="link back">
                            <i class="ti-arrow-left"></i>
                        </a>
                    @else
                        <a onclick="document.location='/categories'" class="link back">
                            <i class="ti-arrow-left"></i>
                        </a>
                    @endif
                </div>
                <div class="title">
                    ລາຍການສິນຄ້າ
                </div>
                <div class="right">
                    <a onclick="document.location='/cart'"><i class="ti-shopping-cart-full">{{ $cartCount }}</i></a>
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- popular menu -->
            <div class="popular-menu segments-page">
                <div class="container">
                    <div class="row">
                        @foreach ($foods as $item)
                            @if ($cartData->where('id', $item->id)->count() > 0)
                                <div class="col-50">
                                    <div class="content">
                                        <img src="{{ $item->image }}" alt="">
                                        <div class="text">
                                            <a href="#">
                                                <h4>{{ $item->name }}</h4>
                                                <h5 style="color:rgb(255, 0, 0)">ເພີ່ມກະຕ່າແລ້ວ</h5>
                                            </a>
                                            <span>{{ $item->code }}</span>
                                            <p class="price">{{ $item->price }}$</p>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <a wire:click='addCart({{ $item->id }})'>
                                    <div class="col-50">
                                        <div class="content">
                                            <img src="{{ $item->image }}" alt="">
                                            <div class="text">
                                                <button wire:click='addCart({{ $item->id }})'>ເພີ່ມກະຕ່າ</button>
                                                <a wire:click='addCart({{ $item->id }})'>
                                                    <h4>{{ $item->name }}</h4>
                                                </a>
                                                <span>{{ $item->code }}</span>
                                                <p class="price">{{ $item->price }}$</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- end popular menu -->
        </div>
    </div>

</div>
