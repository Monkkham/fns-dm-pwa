<div wire:poll.10s>

    <div class="page">
        <div class="navbar navbar-page">
            <div class="navbar-inner sliding">
                <div class="left">
                    <a onclick="document.location='/'" class="link back">
                        <i class="ti-arrow-left"></i>
                    </a>
                </div>
                <div class="title">
                    ລາຍການປະຫວັດ
                </div>
            </div>
        </div>
        <div class="page-content">
            <!-- blog -->
            <div class="blog segments-page">
                <div class="container">
                    @if ($data->count() > 0)
                        @foreach ($data as $item)
                            <div class="row">
                                <div class="col-40">
                                    <div class="content-image">
                                        <img src="{{ $item->onepay_image }}" alt="">
                                    </div>
                                </div>
                                <div class="col-60">
                                    <div class="content-text">
                                        <h4>ລະຫັດ:{{ $item->code }}</h4>
                                        <span>
                                            {{-- format day --}}
                                            {{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y') }}
                                            {{-- format time --}}
                                            {{ \Carbon\Carbon::parse($item->created_at)->format('H:i:s') }}
                                        </span>
                                        <br>
                                        @if ($item->status == 1)
                                            <small
                                                style="background-color: rgb(255, 111, 0);color: rgb(255, 255, 255);">ລໍຖ້າກວດສອບ</small>
                                        @elseif ($item->status == 2)
                                            <small
                                                style="background-color: green;color: rgb(255, 255, 255);">ອານຸມັດເເລ້ວ</small>
                                        @elseif ($item->status == 3)
                                            <small
                                                style="background-color: rgb(212, 255, 0);color: rgb(255, 255, 255);">ຄົວກິນ</small>
                                        @elseif ($item->status == 4)
                                            <small
                                                style="background-color: rgb(0, 13, 255);color: rgb(255, 255, 255);">ກຳລັງເສິບ</small>
                                        @else
                                            <small
                                                style="background-color: green;color: rgb(255, 255, 255);">ສຳແລັດ</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <h5 style="text-align: center">ບໍ່ມີຂໍ້ມູນ</h5>
                    @endif

                </div>
            </div>
            <!-- end blog -->
        </div>
    </div>

</div>
