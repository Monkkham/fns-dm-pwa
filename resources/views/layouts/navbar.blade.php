<div class="navbar navbar-home">
    <div class="navbar-inner">
        <div class="block">
            <div class="row">
                <div class="col-20">
                    <a onclick="document.location='/'" class="link text-color-white">
                        <i class="ti-home"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a onclick="document.location='/categories'" class="link text-color-white">
                        <i class="ti-view-list-alt"></i>
                    </a>
                </div>
                <div class="col-20">
                    <a href="#" class="link text-color-white">
                        <i class="ti-search"></i>
                    </a>

                </div>
                <div class="col-20">
                    <a onclick="document.location='/history'" class="link">
                        <i class="ti-timer"></i>
                    </a>

                </div>
                <div class="col-20">
                    <a onclick="document.location='/menu/0'" class="link text-color-white">
                        <i class="ti-layout-grid2"></i>
                    </a>
                    
                </div>
            </div>
        </div>
    </div>
</div>
