<?php

namespace App\Http\Livewire\Components;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Food extends Component {
    public $food;
    public $cartData;
    public $cartCount;
    public $cartTotal;
    public $checkback;

    function mount( $id ) {
        $this->checkback = $id;
    }

    public function render() {
        $this->cartData = Cart::content();
        $this->cartCount = Cart::count();
        $this->cartTotal = Cart::subTotal();
        $foods = DB::table( 'foods' )->get();
        return view( 'livewire.components.food', compact( 'foods' ) )->layout( 'layouts.auth.style' );
    }
    // add to cart

    public function addCart( $f_id ) {
        $food = DB::table( 'foods' )->where( 'id', $f_id )->first();
        $itemsId = $food->id;
        $itemsName = $food->name;
        $itemsQty = 1;
        $itemsPrice = ( float )$food->price;
        Cart::add( $itemsId, $itemsName, $itemsQty, $itemsPrice );
        toastr()->success( 'successfully!' );
    }
}