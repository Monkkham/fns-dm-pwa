<?php

namespace App\Http\Livewire\Components;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class RegisterContent extends Component
{
    public  $code,
        $name,
        $lastname,
        $gender,
        $phone,
        $email,
        $password,
        $address,
        $village_id,
        $district_id,
        $province_id,
        $roles_id,
        $image,
        $agree,
        $confirm_password;
        public $villages = [];
        public $districts = [];
        public function resetform()
        {
        $this->name = '';
        $this->lastname = '';
        $this->gender = '';
        $this->phone = '';
        $this->email = '';
        $this->password = '';
        $this->address = '';
        $this->village_id = '';
        $this->district_id = '';
        $this->province_id = '';
        $this->roles_id = '';
        $this->image = '';
        $this->confirm_password = '';
        }
        public function moun()
        {
            $this->role_id = 2;
        }
    public function render()
    {
        return view('livewire.components.register-content')->layout( 'layouts.auth.style' );
    }
    protected $rules = [
        'name'=>'required',
        // 'gender'=>'required',
        // 'lastname'=>'required',
        'phone'=>'required|numeric|digits_between:8,8|unique:customer,phone',
        'password'=>'required|min:6',
        // 'province_id'=>'required',
        // 'district_id'=>'required',
        // 'village_id'=>'required',
    ];
    protected $messages = [
        'name.required'=>'ກະລຸນາປ້ອນຊື່ກ່ອນ!',
        // 'gender.required'=>'ເລືອກເພດກ່ອນ!',
        // 'lastname.required'=>'ກະລຸນາປ້ອນນາມສະກຸນກ່ອນ!',
        'phone.required'=>'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
        'phone.numeric'=>'ກະລຸນາປ້ອນເບີໂທເປັນຕົວເລກ!',
        'phone.digits_between'=>'ກະລຸນາປ້ອນເບີໂທ 8 ຕົວເລກ!',
        'phone.unique'=>'ເບີໂທລະສັບນີ້ມີໃນລະບົບເເລ້ວ!',
        'password.required'=>'ກະລຸນາປ້ອນລະຫັດຜ່ານກ່ອນ!',
        'password.min'=>'ລະຫັດຜ່ານ 6 ຕົວຂື້ນໄປ!',
        // 'province_id.required'=>'ກະລຸນາເລືອກແຂວງກ່ອນ!',
        // 'district_id.required'=>'ກະລຸນາເລືອກເມືອງກ່ອນ!',
        // 'village_id.required'=>'ກະລຸນາເລຶືອກບ້ານກ່ອນ!',
    ];
    public function update($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function register()
    {
        $this->validate();
        if($this->password == $this->confirm_password){
            // if($this->agree == 'agree'){
                $buyland_max = User::count('id');
                $count = $buyland_max + 1;
                $customer = User::create([
                    'name'=>$this->name,
                    'code'=>$this->code = 'CUS-00'.$count,
                    'lastname'=>$this->lastname,
                    'gender'=>$this->gender,
                    'phone'=>$this->phone,
                    'password'=>bcrypt($this->password),
                    'village_id'=>$this->village_id,
                    'district_id'=>$this->district_id,
                    'province_id'=>$this->province_id,
                ]);
                $this->resetForm();
                $this->dispatchBrowserEvent('swal:register', [
                    'type' => 'success',  
                    'message' => 'ຍິນດີຕ້ອນຮັບ!', 
                    'text' => 'ລົງທະບຽນສຳເລັດເເລ້ວ'
                ]);
            Auth::guard('web')->login($customer);
                return redirect(route('home'));
            // }else{
            //     session()->flash('agree','ທ່ານຍອມຮັບນະໂຍບາຍຂອງພວກເຮົາບໍ່?');
            // }
        }else{
            session()->flash('no_match','ລະຫັດຜ່ານບໍ່ຕົງກັນ! ລອງໃຫມ່ອີກຄັ້ງ');
        }
    }
}