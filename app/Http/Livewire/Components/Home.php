<?php

namespace App\Http\Livewire\Components;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Home extends Component {
    public $slieImages;
    public $topMenus;
    public $cartData;
    public $cartCount;
    public $cartTotal;

    public function render() {
        $this->cartData = Cart::content();
        $this->cartCount = Cart::count();
        $this->cartTotal = Cart::subTotal();
        $this->slieImages = DB::table( 'slide_photo' )->get();
        $this->topMenus = DB::table( 'foods' )->limit( 8 )->get();
        return view( 'livewire.components.home' )->layout( 'layouts.app' );
    }
    // add to cart

    public function addCart( $f_id ) {
        $food = DB::table( 'foods' )->where( 'id', $f_id )->first();
        $itemsId = $food->id;
        $itemsName = $food->name;
        $itemsQty = 1;
        $itemsPrice = ( float )$food->price;
        Cart::add( $itemsId, $itemsName, $itemsQty, $itemsPrice );
        toastr()->success( 'successfully!' );
    }
}