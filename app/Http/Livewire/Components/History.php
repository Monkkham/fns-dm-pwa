<?php

namespace App\Http\Livewire\Components;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class History extends Component {
    public $data;

    public function render() {
        $this->data = DB::table( 'preorder' )
        ->where( 'customer_id', auth()->user()->id )
        ->get();
        return view( 'livewire.components.history' )->layout( 'layouts.auth.style' );
    }
}
