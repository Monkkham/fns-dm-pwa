<?php

namespace App\Http\Livewire\Components;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Login extends Component {
    public function render() {
        return view( 'livewire.components.login' )->layout( 'layouts.auth.style' );
    }

    public $phone;

    public function _customerLong() {
        $data = DB::table( 'customer' )->where( 'phone', $this->phone )->first();
        if ( $data ) {
            $this->_makeAuth( $this->phone );
        } else {
            $this->validate([
                'phone'=>'required',
            ],[
                'phone.required'=>'ປ້ອນເບີໂທກ່ອນ',
            ]);
            $user = DB::table( 'customer' )->insert( [
                'code' => 'CUS-'.date( 'YmdHis' ),
                'name' => 'ລູກຄ້າ',
                'lastname' => 'ທົ່ວໄປ',
                'phone' => $this->phone,
                'password' => bcrypt( $this->phone ),
            ] );
            if ( $user ) {
                $this->_makeAuth( $this->phone );
            }
        }
    }

    public function _makeAuth( $phone ) {
        if ( auth()->attempt( [ 'phone' => $phone, 'password' => $phone ] ) ) {
            toastr()->success( 'success login' );
            return redirect()->route( 'home' );
        } else {
            toastr()->error( 'error something wrong' );
        }
    }
}