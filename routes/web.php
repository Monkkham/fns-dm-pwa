<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Components\RegisterContent;
use App\Http\Livewire\Components\Food as FoodComponent;
use App\Http\Livewire\Components\Home as HomeComponent;
use App\Http\Livewire\Components\Login as LoginComponent;
use App\Http\Livewire\Components\History as HistoryComponent;
use App\Http\Livewire\Components\CartList as CartListComponent;
use App\Http\Livewire\Components\Category as CategoryComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', LoginComponent::class)->name('login');
Route::get('/register', RegisterContent::class)->name('register');
Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/', HomeComponent::class)->name('home');
    Route::get('/categories', CategoryComponent::class)->name('categories');
    Route::get('/menu/{id}', FoodComponent::class)->name('menu');
    Route::get('/history', HistoryComponent::class)->name('history');
    Route::get('/cart', CartListComponent::class)->name('cart');
});